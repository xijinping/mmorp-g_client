#include "Includes.h"
#include "fcntl.h"

static constexpr std::size_t bufferLen = 2048;
int myCharacter = -1;

static Vector3 toRaylib(const Point& point) {
    return Vector3{point.x, 0, point.y};
}

int sendMovement(int fd, const Addr& server, const Point& point) {
    Data::Movement movement;
    movement.point = point;
    
    Data::ClientMessage msg;
    msg.type = Data::ClientMessage::Type::Move;
    msg.typeData = movement;
    
    std::array<char, bufferLen> buffer;
    yas::mem_ostream os(buffer.data(), bufferLen);
    yas::binary_oarchive<yas::mem_ostream> oa(os);
    oa.serialize(msg);
    
    auto status =
        sendto(fd, buffer.data(), os.size(), 0, (sockaddr*)&server, addrLen);
    return status;
}

int recvMessages(int fd, std::unordered_map<int, Data::Character>& characters) {
    std::array<char, bufferLen> buffer;
    Addr addr;  //server
    socklen_t serverLen = addrLen;
    
    auto status =
        recvfrom(fd, buffer.data(), bufferLen, 0, (sockaddr*)&addr, &serverLen);
    if (status < 0) return status;
    
    Data::ServerMessage msg;
    yas::mem_istream is(buffer.data(), status);
    yas::binary_iarchive<yas::mem_istream> ia(is);
    ia.serialize(msg);
    
    switch(msg.type) {
    case Data::ServerMessage::Type::GiveControl:
    {
        auto* index = std::get_if<int>(&msg.typeData);
        if(!index) break;
        myCharacter = *index;
        break;
    }
    case Data::ServerMessage::Type::SyncCharacter:
    {
        auto* character = std::get_if<Data::Character>(&msg.typeData);
        if(!character) break;
        characters[character->id] = *character;
        break;
    }
    case Data::ServerMessage::Type::SyncChunk:
    {
        auto* chunk = std::get_if<Data::Chunk>(&msg.typeData);
        if(!chunk) break;
        for (auto& character : chunk->characters)
            characters[character.id] = character;
        break;
    }
    default:
        break;
    }
    
    return status;
}

int main(int argc, char** argv) {
    constexpr short port = 5000;
    constexpr auto winWidth = 800;
    constexpr auto winHeight = 600;
    constexpr std::size_t bufferLen = 2048;
    
    std::unordered_map<int, Data::Character> characters;
    
    Addr server;
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(port);
    
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0) return 1;
    fcntl(fd, F_SETFL, O_NONBLOCK);
    
    Data::ClientMessage msg;
    msg.type = Data::ClientMessage::Type::Connect;
    
    std::array<char, bufferLen> buffer;
    yas::mem_ostream os(buffer.data(), bufferLen);
    yas::binary_oarchive<yas::mem_ostream> oa(os);
    oa.serialize(msg);
    
    InitWindow(winWidth, winHeight, "demo");
    
    Camera3D camera {0};
    camera.position = {.0f, 10.f, 10.f};
    camera.target = {.0f, .0f, .0f};
    camera.up = {.0f, 1.f, .0f};
    camera.fovy = 45.0f;
    camera.projection = CAMERA_PERSPECTIVE;
    
    SetTargetFPS(60);
    
    auto status =
        sendto(fd, buffer.data(), os.size(), 0, (sockaddr*)&server, addrLen);
    if (status < 0) return 2;
    
    while (!WindowShouldClose()) {
        
        std::optional<Point> dir = std::nullopt;
        if (IsKeyPressed(KEY_RIGHT)) dir = Point{1, 0};
        else if (IsKeyPressed(KEY_LEFT)) dir = Point{-1, 0};
        else if (IsKeyPressed(KEY_UP)) dir = Point{0, -1};
        else if (IsKeyPressed(KEY_DOWN)) dir = Point{0, 1};
        if(dir) {
            auto& character = characters[myCharacter];
            Point newPos = character.pos;
            newPos.x += dir->x;
            newPos.y += dir->y;
            sendMovement(fd, server, newPos);
        }
        
        while(recvMessages(fd, characters) > 0);
        BeginDrawing();
            ClearBackground(RAYWHITE);
            BeginMode3D(camera);
                for (auto& pair : characters) {
                    auto& character = pair.second;
                    DrawCube(toRaylib(character.pos), 1.f, 1.f, 1.f, RED);
                    DrawCubeWires(toRaylib(character.pos), 1.f, 1.f, 1.f, MAROON);
                }
                DrawGrid(10, 1.f);
            EndMode3D();
            DrawFPS(10, 10);
        EndDrawing();
    }
    CloseWindow();
    
    return 0;
}
