#pragma once

#include "data/Data.h"
#include <arpa/inet.h>

using Point = Data::Point;

using Addr = sockaddr_in;
constexpr auto addrLen = sizeof(Addr);

namespace std {
    template <> struct hash <Addr> {
        size_t operator() (const Addr& obj) const {
            auto addr = obj.sin_addr.s_addr;
            auto port = obj.sin_port;
            auto h0 = hash<decltype(addr)>()(addr);
            auto h1 = hash<decltype(port)>()(port);
            
            h0 += 0x9e3779b9;
            return h0 ^ (
                h1
                + 0x9e3779b9
                + (h0 << 6)
                + (h0 >> 2)
            );
        }
    };
    
    constexpr bool operator==
    (const Addr& rhs, const Addr& lhs){
        return (rhs.sin_addr.s_addr == lhs.sin_addr.s_addr)
            && (rhs.sin_port == lhs.sin_port);
    }
}
